#include "graph.h"
#include <fstream>
#include <algorithm>
#include <string>
using namespace std;

static bool operator<(const Edge e1, const Edge e2){
    return (e1.v<e2.v)&&(e1.w<e2.w);
}

Graph::Graph(){
	N = 0;
}

Graph::Graph(int n){
    N=n;
}

Graph::~Graph(){
	N = 0;
	A_Matrix.Matrix.clear();
	A_List.List.clear();
	LoE.List.clear();
}

void Graph::ReadAMatrix(string filename){
	char c;
	CurrentView = 1;
	ifstream f;
	f.open(filename);
	f >> c >> N >> W;
	A_Matrix.Matrix.resize(N);
	for (int i = 0; i < N; i++){
		A_Matrix.Matrix[i].resize(N);
		for (int j = 0; j < N; j++)
			f >> A_Matrix.Matrix[i][j];
	}
    R=0;
    for (int i=0;i<N;i++)
        for (int j=0;j<N;j++)
            if (A_Matrix.Matrix[i][j]!=A_Matrix.Matrix[j][i]){
                R=1;
                break;
            }
}

void Graph::ReadAList(string filename){
	char c;
	CurrentView = 2;
	ifstream f;
	f.open(filename);
	f >> c >> N >> R >> W;
	for (int i = 0; i < N; i++){
		string str;
		while (!f.eof()){
			getline(f, str);
			string temp = "";
			vector <int> neighbours;
			for (int i = 0; i < str.length(); i++)
				if (str.substr(i, 1) != " ")
					temp += str.substr(i, 1);
				else{
					neighbours.push_back(stoi(temp));
					temp = "";
				}
			if (W == 1){
				int i = 0;
				map<int, int> tmp;
				while (i < neighbours.size()){
					tmp.emplace(neighbours[i], neighbours[i + 1]);
					i += 2;
				}
				A_List.List.push_back(tmp);
			}
			else{
				map<int, int> tmp;
				for (int i = 0; i < neighbours.size(); i++)
					tmp.emplace(neighbours[i], 0);
				A_List.List.push_back(tmp);
			}
		}
	}
    if (R==1)
        for (int i = 0; i < A_List.List.size(); i++)
            for (map<int, int>::iterator it = A_List.List[i].begin(); it != A_List.List[i].end(); it++)
                A_List.List[(*it).first].emplace(i, (*it).second);
}

void Graph::ReadLoE(string filename){
	char c;
	CurrentView = 3;
	ifstream f;
	f.open(filename);

	int M;
	f >> c >> N >> M >> R >> W;
	int v, w, l;
	for (int i = 0; i < M; i++){
		f >> v >> w;
		if (W == 1)
			f >> l;
		else
			l = 0;
		Edge e;
		e.v = v;
		e.w = w;
		LoE.List.insert(make_pair(e, l));
	}
}

void Graph::writeGraph(string filename){
	ofstream f;
	f.open(filename);
	switch (CurrentView)
	{
	case 1:{
		f << "C " << N << " " << W << endl;
		for (int i = 0; i < N; i++){
			for (int j = 0; j < N; j++)
				f << A_Matrix.Matrix[i][j] << " ";
			f << endl;
		}
		break;
	}
	case 2:{
		f << "L " << N << endl;
		f << R << W << endl;
        for (int i=0; i<A_List.List.size();i++){
            for (map<int,int>::iterator it = A_List.List[i].begin(); it!= A_List.List[i].end(); it++)
                f << (*it).first << " " <<(*it).second << " ";
            f << endl;
        }
		break;
	}
	case 3:{
        f << "E " << N << " " << LoE.List.size() << endl;
        f << R << " " << W <<endl;
        for (map<Edge,int>::iterator it=LoE.List.begin();it!=LoE.List.end();it++)
            if (R==1)
                f << (*it).first.v << " " << (*it).first.w << " " << (*it).second << endl;
            else
                f << (*it).first.v << " " << (*it).first.w << endl;
		break;
    }}
}

void Graph::AddAMatrix(Edge e){
	A_Matrix.Matrix[e.v][e.w] = 1;
	if (R == 0)
		A_Matrix.Matrix[e.w][e.v] = 1;
}

void Graph::AddAMatrix(Edge e, int w){
	A_Matrix.Matrix[e.v][e.w] = w;
	if (R == 0)
		A_Matrix.Matrix[e.w][e.v] = w;
}

void Graph::AddAList(Edge e){
	A_List.List[e.v].emplace(e.w,0);
	if (R == 0)
		A_List.List[e.w].emplace(e.v, 0);
}

void Graph::AddAList(Edge e, int w){
	A_List.List[e.v].emplace(e.w, w);
	if (R == 0)
		A_List.List[e.w].emplace(e.v, w);
}

void Graph::AddLoE(Edge e){
	LoE.List.emplace(e, 0);
	if (R == 0){
		Edge tmp;
		tmp.v = tmp.w;
		tmp.w = tmp.v;
		LoE.List.emplace(tmp, 0);
	}
}

void Graph::AddLoE(Edge e, int w){
	LoE.List.emplace(e, w);
	if (R == 0){
		Edge tmp;
		tmp.v = tmp.w;
		tmp.w = tmp.v;
		LoE.List.emplace(tmp, w);
	}
}

void Graph::DeleteAList(Edge e){
	A_List.List[e.v].erase(e.w);
	if (R == 0)
		A_List.List[e.w].erase(e.v);
}

void Graph::DeleteAMatrix(Edge e){
	A_Matrix.Matrix[e.v][e.w] = 0;
	if (R == 0)
		A_Matrix.Matrix[e.w][e.v] = 0;
}

void Graph::DeleteLoE(Edge e){
	LoE.List.erase(e);
	if (R == 0){
		Edge tmp;
		tmp.v = e.w;
		tmp.w = e.v;
		LoE.List.erase(tmp);
	}
}

int Graph::ChangeAMatrix(Edge e, int w){
    int wei = A_Matrix.Matrix[e.v][e.w];
	A_Matrix.Matrix[e.v][e.w] = w;
	if (R == 0)
		A_Matrix.Matrix[e.w][e.v] = w;
    return wei;
}

int Graph::ChangeAList(Edge e, int w){
    int wei = A_List.List[e.v][e.w];
	A_List.List[e.v][e.w] = w;
	if (R == 0)
		A_List.List[e.w][e.v] = w;
    return wei;
}

int Graph::ChangeLoE(Edge e, int w){
    int wei=LoE.List[e];
    LoE.List[e] = w;
	if (R == 0){
		Edge tmp;
		tmp.v = e.w;
		tmp.w = e.v;
		LoE.List[tmp] = w;
	}
    return wei;
}

void Graph::AdjMatrixToAdjList(){
    map<int,int> tmp;
    A_List.List.clear();
    for (int i=0; i<N;i++){
        for (int j=0;j<N;i++)
            if ((W=1)&&(A_Matrix.Matrix[i][j]!=0))
                tmp.emplace(j,A_Matrix.Matrix[i][j]);
            else if ((W==0)&&(A_Matrix.Matrix[i][j]!=0))
                tmp.emplace(j,0);
        A_List.List.push_back(tmp);
        tmp.clear();
    }
    A_Matrix.Matrix.clear();
}

void Graph::AdjMatrixToLoE(){
    LoE.List.clear();
    Edge e;
    for (int i=0; i< N; i++)
        for (int j=1;j<N;j++){
            e.v=i;
            e.w=j;
            if ((W==1)&&(A_Matrix.Matrix[i][j]!=0))
                LoE.List.emplace(e,A_Matrix.Matrix[i][j]);
            else if ((W==0)&&(A_Matrix.Matrix[i][j]!=0))
                LoE.List.emplace(e,0);
        }
    A_Matrix.Matrix.clear();
}

void Graph::AdjListToAdjMatrix(){
    A_Matrix.Matrix.clear();
    A_Matrix.Matrix.resize(N);
    for (int i=0;i<N;i++)
        A_Matrix.Matrix[i].resize(N);
    for (int i=0;i<A_List.List.size();i++){
        A_Matrix.Matrix[i].resize(N);
        for (map<int,int>::iterator it=A_List.List[i].begin();it!=A_List.List[i].end();it++)
            if (W==1)
                A_Matrix.Matrix[i][(*it).first]=(*it).second;
            else
                A_Matrix.Matrix[i][(*it).first]=1;
    }
    A_List.List.clear();
}

void Graph::AdjListToLoE(){
    LoE.List.clear();
    Edge e;
    for (int i=0;i<A_List.List.size();i++)
        for (map<int,int>::iterator it=A_List.List[i].begin();it!=A_List.List[i].end();it++){
            e.v=i;
            e.w=(*it).first;
            LoE.List.emplace(e,(*it).second);
        }
    A_List.List.clear();
}

void Graph::LoEToAdjMatrix(){
    A_Matrix.Matrix.clear();
    A_Matrix.Matrix.resize(N);
    for (int i=0;i<N;i++)
        A_Matrix.Matrix[i].resize(N);
    for (map<Edge,int>::iterator it=LoE.List.begin();it!=LoE.List.end();it++)
        if (W==1)
            A_Matrix.Matrix[(*it).first.v][(*it).first.w]=(*it).second;
        else
            A_Matrix.Matrix[(*it).first.v][(*it).first.w]=1;
}

void Graph::LoEToAdjList(){
    A_List.List.clear();
    A_List.List.resize(N);
    map<int,int> tmp;
    for (int i=0;i<N;i++)
        A_List.List.push_back(tmp);
    for (map<Edge,int>::iterator it=LoE.List.begin();it!=LoE.List.end();it++)
        A_List.List[(*it).first.v].emplace((*it).first.w, (*it).second);
}

void Graph::addEdge(Edge e){
	switch (CurrentView){
	case 1:{
		AddAMatrix(e);
		break;
	}
	case 2:{
		AddAList(e);
		break;
	}
	case 3:{
		AddLoE(e);
	}
	}
}

void Graph::addEdge(Edge e, int w){
	switch (CurrentView){
	case 1:{
		AddAMatrix(e,w);
		break;
	}
	case 2:{
		AddAList(e,w);
		break;
	}
	case 3:{
		AddLoE(e,w);
	}
	}
}

void Graph::removeEdge(Edge e){
	switch (CurrentView){
	case 1:{
		DeleteAMatrix(e);
		break;
	}
	case 2:{
		DeleteAList(e);
		break;
	}
	case 3:{
		DeleteLoE(e);
		break;
	}
	}
}

int Graph::changeEdge(Edge e, int w){
    int res=0;
	switch (CurrentView){
	case 1:{
		res= ChangeAMatrix(e, w);
		break;
	}
	case 2:{
		res= ChangeAList(e, w);
		break;
	}
	case 3:{
		res= ChangeLoE(e, w);
		break;
	}
    }
    return res;
}


void Graph::transformToAdjMatrix(){
    switch (CurrentView){
        case 2:{
            AdjListToAdjMatrix();
            break;
        }
        case 3:{
            LoEToAdjMatrix();
            break;
        }
    }
}

void Graph::transformToAdjList(){
    switch (CurrentView){
        case 1:{
            AdjMatrixToAdjList();
            break;
        }
        case 3:{
            LoEToAdjList();
            break;
        }
    }
}

void Graph::transformToListOfEdges(){
    switch (CurrentView) {
        case 1:{
            AdjMatrixToLoE();
            break;
        }
        case 2:{
            AdjListToLoE();
            break;
        }
    }
}

void Graph::readGraph(string filename){
    char tmp;
    ifstream f;
    f.open(filename);
    f >> tmp;
    f.close();
    switch (tmp){
        case 'C':{
            ReadAMatrix(filename);
            break;
        }
        case 'L':{
            ReadAList(filename);
            break;
        }
        case 'E':{
            ReadLoE(filename);
            break;
        }
    }
}

DSU::DSU(int n){
    p.resize(n);
    for (int i=0;i<n;i++)
        p[i]=i;
    
}

int DSU::Find(int x){
    return ( x == p[x] ? x : p[x] = Find(p[x]) );
}

void DSU::Union(int x, int y)
{
    if ( (x = Find(x)) == (y = Find(y)) )
        return;
    else{
        swap(x,y);
        if (x!=y)
            p[x]=y;
    }
}

struct CompareWeight{
bool operator()(const pair<Edge,int>& left, const pair<Edge,int>& right)
{
    return left.second < right.second;
}
};

Graph Graph::getSpaingTreePrima(){
    int prevType = CurrentView;
    transformToListOfEdges();
    Graph Tree(N);
    vector <int> inPoints, outPoints;
    ListOfEdges current;
    current.List.clear();
    inPoints.push_back(1);
    for (int i=2;i<=N;i++)
        outPoints.push_back(i);
    for (int i=1;i<N;i++){
        for (map<Edge,int>::iterator it = LoE.List.begin();it!=LoE.List.end();it++)
            if (((find(inPoints.begin(),inPoints.end(),(*it).first.v)!=inPoints.end())&&(find(outPoints.begin(),outPoints.end(),(*it).first.w)!=outPoints.end()))
                ||((find(outPoints.begin(),outPoints.end(),(*it).first.v)!=outPoints.end())&&(find(inPoints.begin(),inPoints.end(),(*it).first.w)!=inPoints.end())))
                current.List.emplace((*it).first,(*it).second);
        pair<Edge,int> target = *min_element(current.List.begin(),current.List.end(),CompareWeight());
        if (find(inPoints.begin(),inPoints.end(),target.first.v)!=inPoints.end()){
            inPoints.push_back(target.first.w);
            outPoints.erase(find(outPoints.begin(),outPoints.end(),target.first.w));
        }
        else{
            inPoints.push_back(target.first.v);
            outPoints.erase(find(outPoints.begin(),outPoints.end(),target.first.v));

        }
        Tree.addEdge(target.first,target.second);
        switch (prevType)
        {
            case 1:{
                transformToAdjMatrix();
                break;
            }
            case 2:{
                transformToAdjList();
                break;
            }
            case 3:{
                transformToListOfEdges();
                break;
            }
        }
    }
    return Tree;
}

Graph Graph::getSpaingTreeKruscal(){
    int prevType = CurrentView;
    transformToListOfEdges();
    Graph Tree(N);
    DSU myDSU(N);
    ListOfEdges edges = LoE;
    while (edges.List.size()>0){
        pair<Edge,int> target = *min_element(edges.List.begin(),edges.List.end(),CompareWeight());
        if (myDSU.Find(target.first.v)!=myDSU.Find(target.first.w)){
            Tree.addEdge(target.first,target.second);
            myDSU.Union(target.first.v, target.first.w);
        }
        Edge e;
        e.v=target.first.v;
        e.w=target.first.w;
        edges.List.erase(e);
    }
    switch (prevType)
    {
        case 1:{
            transformToAdjMatrix();
            break;
        }
        case 2:{
            transformToAdjList();
            break;
        }
        case 3:{
            transformToListOfEdges();
            break;
        }
    }
    return Tree;
}

Graph Graph::getSpaingTreeBoruvka(){
    int prevType = CurrentView;
    transformToListOfEdges();
    Graph Tree(N);
    DSU myDSU(N);
    while (Tree.LoE.List.size()!=N-1){
        vector <int> components(N);
        for (int i=0; i<N; i++)
            if (find(components.begin(),components.end(),myDSU.Find(i))!=components.end())
                components.push_back(myDSU.Find(i));
        int component = *components.begin();
        components.erase(components.begin());
        ListOfEdges Edges;
        for (map<Edge,int>::iterator it = LoE.List.begin();it!=LoE.List.end();it++)
            if (((myDSU.Find((*it).first.v)==component)&&(find(components.begin(),components.end(),(*it).first.w)!=components.end()))||((myDSU.Find((*it).first.w)==component)&&(find(components.begin(),components.end(),(*it).first.v)!=components.end())))
                Edges.List.emplace((*it).first,(*it).second);
        pair<Edge,int> target = *min_element(Edges.List.begin(),Edges.List.end(),CompareWeight());
        int newelement;
        if (myDSU.Find(target.first.v)==component)
            newelement = target.first.w;
        else
            newelement = target.first.v;
        myDSU.Union(component, newelement);
        Tree.addEdge(target.first,target.second);
    }
    switch (prevType)
    {
        case 1:{
            transformToAdjMatrix();
            break;
        }
        case 2:{
            transformToAdjList();
            break;
        }
        case 3:{
            transformToListOfEdges();
            break;
        }
    }
    return Tree;
}

int Graph::checkEuler(bool &circleExist){
    int numOdd = 0, start;
    int prevType = CurrentView;
    transformToListOfEdges();
    vector <int> CountEdges(N,0);
    DSU myDSU(N);
    for (map<Edge,int>::iterator it=LoE.List.begin();it!=LoE.List.end();it++){
        CountEdges[(*it).first.v]+=1;
        CountEdges[(*it).first.w]+=1;
        myDSU.Union((*it).first.v, (*it).first.w);
    }
    for (int i=0;i<N;i++){
        if (CountEdges[i]%2==1){
            numOdd++;
            start=i;
        }
    }
    vector <int> components;
    for (int i=0; i<N; i++)
        if (find(components.begin(),components.end(),myDSU.Find(i))==components.end())
            components.push_back(myDSU.Find(i));
    switch (prevType)
    {
        case 1:{
            transformToAdjMatrix();
            break;
        }
        case 2:{
            transformToAdjList();
            break;
        }
        case 3:{
            transformToListOfEdges();
            break;
        }
    }

    if (components.size()>1)
        if (numOdd==0){
            circleExist=true;
            return 0;
        }
        else
            if (numOdd==2){
                circleExist=false;
                return start;
            }
            else{
                circleExist=false;
                return -1;
            }
    else
        if (numOdd==0){
            circleExist=true;
            return 0;
        }
        else if (numOdd==2){
            circleExist=false;
            return start;
        }
        else{
            circleExist=false;
            return -1;
        }
}

Graph::Graph(Graph const &g){
    N = g.N;
    W = g.W; //Взвешенный
    R = g.R; //ориентированный
    CurrentView = g.CurrentView;
    A_Matrix = g.A_Matrix;
    A_List = g.A_List;
    LoE = g.LoE;

}
bool Graph::isBridge(Edge e){
    Graph tmpGraph(*this);
    tmpGraph.transformToAdjList();
    tmpGraph.removeEdge(e);
    swap(e.v,e.w);
    tmpGraph.removeEdge(e);
    swap(e.v,e.w);
    vector <int> Queue;
    vector <bool> Used;
    for (int i=0; i<tmpGraph.N;i++)
        Used.push_back(false);
    Queue.push_back(e.v);
    Used[e.v]=true;
    bool f=true;
    while (f){
        for (map<int,int>::iterator it=tmpGraph.A_List.List[Queue[0]].begin();it!=tmpGraph.A_List.List[Queue[0]].end();it++){
            if ((*it).first==e.w)
                return false;
            if (Used[(*it).first]) continue;
            Queue.push_back((*it).first);
            Used[(*it).first]=true;
        }
        Queue.erase(Queue.begin());
        if (Queue.size()==0)
            f=false;
    }
    return true;
}

vector <int> Graph::getEuleranTourFleri(){
    bool exists = false;
    int currV = checkEuler(exists);
    if (currV==-1) return vector<int>(0);
    vector<int> result;
    result.push_back(currV);
    Graph tmpGraph(*this);
    tmpGraph.transformToAdjList();
    map<int,int>::iterator it = tmpGraph.A_List.List[currV].begin();
    while (tmpGraph.A_List.List[currV].size()>0){
        Edge currEdge;
        currEdge.v=currV;
        currEdge.w=(*it).first;
        if (isBridge(currEdge) && tmpGraph.A_List.List[currV].size()>1){
            it++;
            continue;
        }
        result.push_back(currEdge.w);
        currV=currEdge.w;
        it = tmpGraph.A_List.List[currV].begin();
        tmpGraph.removeEdge(currEdge);
        swap(currEdge.v,currEdge.w);
        tmpGraph.removeEdge(currEdge);
        swap(currEdge.v, currEdge.w);
    }
    return result;
}

vector<int> Graph::getEuleranTourEffective(){
    bool exists = false;
    int currV = checkEuler(exists);
    if (currV==-1) return vector<int>(0);
    vector<int> result;
    result.push_back(currV);
    Graph tmpGraph(*this);
    tmpGraph.transformToAdjList();
    EuleranTourRecursive(currV, tmpGraph, result);
    return result;
}

void Graph::EuleranTourRecursive(int v, Graph &g, vector<int> &result){
    while (g.A_List.List[v].size()!=0){
        map<int,int>::iterator it = g.A_List.List[v].begin();
        Edge e;
        e.v=v;
        e.w=(*it).first;
        g.removeEdge(e);
        swap(e.v, e.w);
        g.removeEdge(e);
        swap(e.v, e.w);
        EuleranTourRecursive(e.w, g, result);
    }
    result.push_back(v);
}

int Graph::checkBipart(vector <char> &marks){
    int prevType = CurrentView;
    transformToAdjList();
    marks.resize(N, -1);
    bool ok = true;
    vector <int> q(N);
    for (int st=0;st<N;st++){
        if (marks[st]==-1){
            int h=0,t=0;
            q[t++]=st;
            marks[st]=0;
            while (h<t){
                int v = q[h++];
                for (map<int,int>::iterator it=A_List.List[v].begin(); it!=A_List.List[v].end();it++){
                    int to = (*it).first;
                    if (marks[to] == -1)
                        marks[to] = !marks[v], q[t++] = to;
                    else
                        ok &= marks[to] != marks[v];
                }
            }
        }
    }
    switch (prevType)
    {
        case 1:{
            transformToAdjMatrix();
            break;
        }
        case 2:{
            transformToAdjList();
            break;
        }
        case 3:{
            transformToListOfEdges();
            break;
        }
    }
    if (ok)
        return 1;
    else
        return 0;
}

bool Graph::try_kuhn(int v, vector <char> marks, vector<char> &used, vector<int> &mt){
    if (used[v]) return false;
    used[v]=true;
    for (map<int,int>::iterator it=A_List.List[marks[v]].begin();it!=A_List.List[marks[v]].end();it++){
        if (marks[v]!=marks[(*it).first]){
            int to = (*it).first;
            if (mt[to] == -1 || try_kuhn(mt[to], marks, used, mt)){
                mt[to]=v;
                return true;
            }
        }
    }
    return false;
}

vector<pair<int,int>> Graph::getMaximumMatchingBipart(){
    int prevType = CurrentView;
    transformToAdjList();
    vector <char> marks, used;
    vector <int> mt;
    vector <pair<int,int>> result;
    if (checkBipart(marks)==1){
        mt.assign(N, -1);
        for (int i=0;i<N;i++){
            if (marks[i]==0){
                used.assign(N, false);
                try_kuhn(i, marks, used, mt);
            }
        }
        for (int i=0;i<N;i++){
            if (mt[i]!=-1)
                result.push_back(make_pair(mt[i],i));
        }
        return result;
    }
    else return vector<pair<int,int>>();
}