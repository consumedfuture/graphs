//
//  main.cpp
//  lab0
//
//  Created by Александр Лычагов on 01.06.15.
//  Copyright (c) 2015 Александр Лычагов. All rights reserved.
//

#include "graph.h"

int main(int argc, const char * argv[]) {
    Graph g;
    g.readGraph("input.txt");
    g.transformToAdjList();
    g.writeGraph("output.txt");
    system("pause");
    return 0;
}
