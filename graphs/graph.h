#include <iostream>
#include <vector>
#include <list>
#include <map>
using namespace std;

struct Edge{
	int v, w;
};

struct AdjMatrix{
	vector <vector <int>> Matrix;
};

struct AdjList{
	vector <map <int, int>> List;
};

struct ListOfEdges{
	map <Edge, int> List;
};

class Graph{
private:
	int N; //Число вершин
	bool W; //Взвешенный
	bool R; //ориентированный
	int CurrentView;
	AdjMatrix A_Matrix;
	AdjList A_List;
	ListOfEdges LoE;
	void AddAMatrix(Edge e);
	void AddAMatrix(Edge e, int w);
	void AddAList(Edge e);
	void AddAList(Edge e, int w);
	void AddLoE(Edge e);
	void AddLoE(Edge e, int w);
	void DeleteAMatrix(Edge e);
	void DeleteAList(Edge e);
	void DeleteLoE(Edge e);
	int ChangeAMatrix(Edge e, int w);
	int ChangeAList(Edge e, int w);
	int ChangeLoE(Edge e, int w);
	void ReadAMatrix(string filename);
	void ReadAList(string filename);
	void ReadLoE(string filename);
    void AdjMatrixToAdjList();
    void AdjMatrixToLoE();
    void AdjListToAdjMatrix();
    void AdjListToLoE();
    void LoEToAdjMatrix();
    void LoEToAdjList();
    bool isBridge(Edge e);
    void EuleranTourRecursive(int v, Graph &g, vector<int> &result);
    bool try_kuhn(int v, vector <char> marks, vector<char> &used, vector<int> &mt);
public:
	Graph();
    Graph(int n);
    Graph(Graph const &g);
	~Graph();
	void addEdge(Edge e);
	void addEdge(Edge e, int w);
	void removeEdge(Edge e);
	int changeEdge(Edge e, int w);
	void readGraph(string filename);
	void writeGraph(string filename);
	void transformToAdjMatrix();
	void transformToAdjList();
	void transformToListOfEdges();
    Graph getSpaingTreePrima();
    Graph getSpaingTreeKruscal();
    Graph getSpaingTreeBoruvka();
    int checkEuler(bool &circleExist);
    vector<int> getEuleranTourFleri();
    vector<int> getEuleranTourEffective();
    int checkBipart(vector <char> &marks);
    vector <pair <int,int>> getMaximumMatchingBipart();
};

class DSU{
    vector<int> p;
public:
    DSU(int n);
    void MakeSet(int x);
    int Find(int x);
    void Union(int x, int y);
};

